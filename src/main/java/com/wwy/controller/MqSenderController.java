package com.wwy.controller;

import com.wwy.MQ.ActiveMQ.JmsProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Destination;
import javax.servlet.http.HttpServletRequest;

@RestController
public class MqSenderController {

    @Autowired
    Destination queue1;

    @Autowired
    JmsProducerService queueJmsProducerServiceImpl;

     @RequestMapping("/JmsTest1.do")
    @ResponseBody   //可以返回Json字符串
    public String JmsTest1(HttpServletRequest request) {
        queueJmsProducerServiceImpl.sendMessage(queue1,"JmsTest1 Producer 消息1");
        return "JmsTest1 Producer 消息1";
    }


}
