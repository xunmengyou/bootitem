
package com.wwy.controller;
import com.alibaba.fastjson.JSON;
import com.wwy.service.Impl.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


@RestController
@RequestMapping("/role")
public class RoleController
{
    @Autowired
    private RoleService roleService;

    @PostMapping("/updateRolePermissions")
    String updateRolePermissions(HttpServletRequest request) {
        return roleService.updateRolePermissions("role1",new ArrayList<>());
    }
    
    @PostMapping("getGrantRolePermTree")
    String getGrantRolePermTree(HttpServletRequest request) {
        return JSON.toJSONString(roleService.getGrantRolePermTree("role1"));
    }
}

