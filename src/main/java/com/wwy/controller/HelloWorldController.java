/**
 * 
 */
package com.wwy.controller;

import com.wwy.service.Impl.RedisWayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangwy
 *
 */

@RestController
public class HelloWorldController {

	@Autowired
	RedisWayServiceImpl redisWayService;

	@RequestMapping("/")  
	   public String sayHello() {  
	       return "Hello,World!";  
	}

	@RequestMapping("/getStringRedis")
	public Object getPersonById(){
		redisWayService.processStringTest();
        return "getStringRedis!";
    }
}
