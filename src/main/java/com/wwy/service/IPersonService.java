package com.wwy.service;

import com.wwy.Entity.Person;

public interface IPersonService {
    Person getPersonById(Integer id);
    void insertPerson(String userId, Person person);
    void delPerson(String userId, Person person);

}
