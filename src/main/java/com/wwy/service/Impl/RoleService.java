
  
package com.wwy.service.Impl;

import com.wwy.Dao.RoleMapper;
import com.wwy.Entity.PermissionRelationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class RoleService{
    private static final Logger logger = LoggerFactory.getLogger(RoleService.class);
    private static final String CACHE_NAME = "RoleCache";
    
    @Resource
    private RoleMapper roleMapper;

   @CacheEvict(value = CACHE_NAME, key = "#roleId")
   public String updateRolePermissions(String roleId,List<String> listId) {
       
       String message = "000";
       try{
           roleMapper.insertRolePermissions(roleId,listId);
       }catch (Exception e){
           logger.error("编辑角色权限失败" + e.getMessage());
           message ="001";
       }        
       return message;
   }
   
   @Cacheable(value = CACHE_NAME, key = "#roleId")
   public List<PermissionRelationship> getGrantRolePermTree(String roleId) {
        List<PermissionRelationship> listPer = null;
        try {
           List<String> listPId = roleMapper.getPermissionIdByRoleId(roleId);       
           listPer = roleMapper.getPermissionList();
           for(int i = 0;i < listPId.size(); i++) {
               String perId = listPId.get(i);         
               for(int j = 0; j < listPer.size(); j++) {
                   if(listPer.get(j).getId().equals(perId)) {
                      listPer.get(j).setHasRole(1);//表示这个角色拥有该权限
                   }
               }
           }
           }catch (Exception e){
               e.printStackTrace();
               logger.error("获取角色的可授权的权限树" + e.getMessage());
           }
        return listPer;
   }
    
}

