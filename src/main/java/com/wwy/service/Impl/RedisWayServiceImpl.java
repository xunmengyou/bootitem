package com.wwy.service.Impl;

import com.wwy.Redis.StringRedisCacheWorker;
import com.wwy.service.RedisWayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedisWayServiceImpl implements RedisWayService {
    @Autowired
    StringRedisCacheWorker stringRedisCacheWorker;

    @Override
    public void processStringTest() {
        stringRedisCacheWorker.write("key_string");
    }
}
