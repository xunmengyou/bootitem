package com.wwy.MQ.ActiveMQ;

import javax.jms.Destination;

public interface JmsProducerService {
    public void sendMessage(Destination destination, final String msg);
    public void sendMessage(final String msg);
}
