package com.wwy.MQ.ActiveMQ;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.*;

public class TopicJmsProducerServiceImpl implements JmsProducerService {

    private JmsTemplate jmsTemplate;

    public void setJmsTemplate(JmsTemplate jmsTemplate){
        this.jmsTemplate = jmsTemplate;
    }
    @Override
    public void sendMessage(Destination destination, final String msg) {
        System.out.println("向主题Topic--"+destination.toString()+"--发送主题消息--"+msg);
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
//                return session.createTextMessage(msg);
                //在一些版本，如果没有调用setText方法，会出现发送消息成功了，但是消费者消费失败的情况，
                // 最终的现象就是消费者连接数变成了0（消费者不断地重连，不断地消费失败）
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText(msg);
                return textMessage;
            }
        });
    }

    @Override
    public void sendMessage(String msg) {

    }
}
