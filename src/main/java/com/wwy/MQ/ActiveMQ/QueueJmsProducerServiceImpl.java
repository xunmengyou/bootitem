package com.wwy.MQ.ActiveMQ;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.jms.*;

@Service
public class QueueJmsProducerServiceImpl implements JmsProducerService {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Override
    public void sendMessage(Destination destination, final String msg) {
        System.out.println("向队列--"+destination.toString()+"--发送消息--"+msg);
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
//                return session.createTextMessage(msg);
                //在一些版本，如果没有调用setText方法，会出现发送消息成功了，但是消费者消费失败的情况，
                // 最终的现象就是消费者连接数变成了0（消费者不断地重连，不断地消费失败）
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText(msg);
                return textMessage;
            }
        });
    }

    @Override
    public void sendMessage(final String msg) {
        System.out.println("向队列--"+jmsTemplate.getDefaultDestinationName().toString()+"--发送消息--"+msg);
        jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText(msg);
                return textMessage;
            }
        });
    }
}
