
package com.wwy.Dao;

import com.wwy.Entity.PermissionRelationship;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
 /**
  * 插入角色的新权限
  */
 void insertRolePermissions(@Param("roleId") String roleId, @Param("listPeId") List<String> listPeId);
 List<PermissionRelationship> getPermissionList();
 List<String> getPermissionIdByRoleId(String roleId);

}
