package com.wwy.springboot;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedisPool;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableCaching
public class ConfigurationUtl extends CachingConfigurerSupport {

    private Logger logger = LoggerFactory.getLogger(ConfigurationUtl.class);

    @Value("${spring.redis.cluster.nodes}")
    private String nodes;

    @Value("${spring.redis.lettuce.pool.max-active}")
    private Integer maxActive;

    @Value("${spring.redis.lettuce.pool.max-idle}")
    private Integer maxIdle;

    @Value("${spring.redis.lettuce.pool.min-idle}")
    private Integer minIdle;

    @Value("${spring.redis.lettuce.pool.max-wait}")
    private String maxWaitMillis;

    @Value("${spring.activemq.broker-url}")
    private String mqBrokerUrl;

    @Value("${spring.activemq.user}")
    private String mqUser;

    @Value("${spring.activemq.password}")
    private String mqPassword;

    @Value("${spring.queue1}")
    private String queue1;

    @Bean("shardedJedisPool")
    public ShardedJedisPool redisShardPoolFactory(){

        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig(); //Jedis池配置
        jedisPoolConfig.setMaxIdle(maxIdle);
        long maxWM = Long.valueOf(maxWaitMillis.substring(0,maxWaitMillis.length()-2));
        jedisPoolConfig.setMaxWaitMillis(maxWM);
        jedisPoolConfig.setMaxTotal(maxActive);
        jedisPoolConfig.setMinIdle(minIdle);
        jedisPoolConfig.setTestOnBorrow(true);

        //支持多个Redis的配置--即Redis集群
        String[] modeStr = nodes.split(",");
        List<JedisShardInfo> listshardedJedisInfo = new ArrayList<JedisShardInfo>();
        for(int i = 0; i<modeStr.length;i++){
            String[] node = modeStr[i].split(":");
            listshardedJedisInfo.add(new JedisShardInfo(node[0],node[1]));
        }
        ShardedJedisPool shardedJedisPool = new ShardedJedisPool(jedisPoolConfig,listshardedJedisInfo);
        logger.info("shardedJedisPool注入成功！");
        return  shardedJedisPool;
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(mqBrokerUrl);
        connectionFactory.setUserName(mqUser);
        connectionFactory.setPassword(mqPassword);
        return connectionFactory;
    }

    @Bean("jmsTemplate")
    public JmsTemplate JmsTemplate(){
        return  new JmsTemplate(connectionFactory());
    }

    @Bean("jmsMessagingTemplate")
    public JmsMessagingTemplate jmsMessageTemplate() {
        return new JmsMessagingTemplate(connectionFactory());
    }

    /**
     * 同时支持topic与queue
     */
    @Bean
    public JmsListenerContainerFactory<?> jmsListenerContainerTopic(ConnectionFactory activeMQConnectionFactory) {
        DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
        bean.setPubSubDomain(true);
        bean.setConnectionFactory(activeMQConnectionFactory);
        return bean;
    }

    @Bean("queue1")
    public Destination queue1(){
        return new ActiveMQQueue(queue1);
    }

    @Bean
    public Destination topic(){
        return new ActiveMQTopic();
    }

}
