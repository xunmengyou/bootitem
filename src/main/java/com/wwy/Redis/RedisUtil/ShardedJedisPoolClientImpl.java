package com.wwy.Redis.RedisUtil;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import javax.annotation.Resource;

@Component("shardedJedisPoolClient")
public class ShardedJedisPoolClientImpl implements ShardedJedisPoolClient{

    @Resource(name = "shardedJedisPool")
    private ShardedJedisPool shardedJedisPool;


    @Override
    public <T> T execute(ShardedJedisOperator<T> parameter) {

        ShardedJedis shardedJedis = null;
        T object;
        try{
            shardedJedis = shardedJedisPool.getResource();
            object = parameter.operator(shardedJedis);
            return object;
        }catch(Exception e){

        }finally {
            if(null != shardedJedis){
                shardedJedis.close();
            }
        }
        return null;
    }
}
