package com.wwy.Redis.RedisUtil;

import org.springframework.stereotype.Component;
import redis.clients.jedis.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;


@Component("jedisCacheUtil")
public class JedisCacheUtil extends RedisCommonUtil {

    @Resource(name = "shardedJedisPoolClient")
    private ShardedJedisPoolClient shardedJedisPoolClient;

    /**------------通用的操作方法  支持所有类型-------------**/

    public long del(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.del(key);
            }
        });
    }

    public boolean exists(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Boolean>() {
            @Override
            public Boolean operator(ShardedJedis sJedis) {
                return sJedis.exists(key);
            }
        });
    }
    //设置失效时间
    public Long expire(final String key, final int seconds) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.expire(key, seconds);
            }
        });
    }
    //移除key的失效时间
    public Long persist(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.persist(key);
            }
        });
    }

    public String type(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.type(key);
            }
        });
    }
    //以秒为单位返回key剩余时间(-2表示key不存在，-1表示永远不过时)
    public long ttl(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.ttl(key);
            }
        });
    }



    /**------------String类型-------------**/
    public String setex(final String key, final String value, final int time) {

        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.setex(key, time, value);
            }
        });
    }

    public String setexBin(final String key, final Object object, final int time) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.setex(key, time, jsonEncode(object));
            }
        });
    }

    public String set(final String key, final String value) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.set(key, value);
            }
        });
    }

    public String setBin(final String key, final Object object) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.set(key, jsonEncode(object));
            }
        });
    }

    public String get(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.get(key);
            }
        });
    }

    public <T> T getBin(final String key, final Class<T> type) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<T>() {
            @Override
            public T operator(ShardedJedis sJedis) {
                return jsonDecode(sJedis.get(key),type);
            }
        });
    }

    //String的追加数据，如果当前key的value有值则附加到原有string后面，如果没有则写入
    public Long append(final String key, final Object object) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.append(key, jsonEncode(object));
            }
        });
    }
    // 获取原有value值的同时写入新的value值
    public String getSet(String key,String value){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.getSet(key,value);
            }
        });
    }
    //判断原值是否存在，存在不赋值，返回0；不存在才赋值，返回1；命令setnx
    public Long setnx(String key,String value){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.setnx(key,value);
            }
        });
    }
    //字符串替换赋值，从指定位置开始替换，命令setrange
    public Long setrange(String key,long offset,String value){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.setrange(key,offset,value);
            }
        });
    }
    //截取字符串，从下标为n开始截取到n或n+1，getrange
    public String getrange(String key,long start,long end){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.getrange(key,start,end);
            }
        });
    }
    //获取指定key对应的值的长度
    public Long strlen(String key){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.strlen(key);
            }
        });
    }

    public Long incr(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.incr(key);
            }
        });
    }

    /**
     * 将 key 中储存的数字值加一。 如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 incr操作
     *
     * @param key 缓存key
     * @param time 缓存时间(秒)
     */
    public long incrBy(final String key, final int time, final Long cnt) {

        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                boolean exist = true;
                long leftTime = sJedis.ttl(key);
                if (leftTime < 0) {
                    // 过期时间小于0说明key不存在
                    exist = false;
                }
                long num = sJedis.incrBy(key, cnt);
                if (!exist) {
                    // 有效时间不为0且key为新插入则设置业务缓存的有效时间
                    sJedis.expire(key, time);
                }
                return num;
            }
        });
    }

    /**
     * 将 key 中储存的数字值加一。 如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 incr操作
     *
     * @param key 缓存key
     * @param time 缓存时间(秒)
     */
    public long incr(final String key, final int time) {

        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                boolean exist = true;
                long leftTime = sJedis.ttl(key);
                if (leftTime < 0) {
                    // 过期时间小于0说明key不存在
                    exist = false;
                }
                long num = sJedis.incr(key);
                if (!exist) {
                    // 有效时间不为0且key为新插入则设置业务缓存的有效时间
                    sJedis.expire(key, time);
                }
                return num;
            }
        });
    }

    public Long decr(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.decr(key);
            }
        });
    }

    /**
     * 同redis的decr命令,如果是第一次插入值，则同时设置在多少秒后失效
     *
     * @param key 键
     * @param time 秒
     */
    public long decr(final String key, final int time) {
        boolean exist = true;
        long leftTime = ttl(key);
        if (leftTime < 0) {
            exist = false;
        }
        Long ret = decr(key);
        if (!exist) {
            expire(key, time);
        }
        return ret;
    }


    /**------------hash类型-------------**/
    //获取指定key中filed字段值
    public String hget(final String key, final String field) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.hget(key, field);
            }
        });
    }

    public <T> T hgetBin(final String key, final String field, final Class<T> type) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<T>() {
            @Override
            public T operator(ShardedJedis sJedis) {
                return jsonDecode(sJedis.hget(key, field),type);
            }
        });
    }

    public boolean hexists(final String key, final String field) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Boolean>() {
            @Override
            public Boolean operator(ShardedJedis sJedis) {
                return sJedis.hexists(key, field);
            }
        });
    }

    //给指定key添加元素
    public Long hset(final String key, final String field, final String value) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.hset(key, field, value);
            }
        });
    }

    public Long hsetBin(final String key, final String field, final Object object) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.hset(key, field, jsonEncode(object));
            }
        });
    }

    public Long hsetnx(final String key, final String field, final Object object) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.hsetnx(key, field, jsonEncode(object));
            }
        });
    }


    public Long hdel(final String key, final String field) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.hdel(key, field);
            }
        });
    }

    public Map<String, String> hgetAll(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Map<String, String>>() {
            @Override
            public Map<String, String> operator(ShardedJedis sJedis) {
                return sJedis.hgetAll(key);
            }
        });
    }

    public <T> Map<String, T> hgetAllBin(final String key, final Class<T> type) {
        Map<String, String> result = shardedJedisPoolClient.execute(new ShardedJedisOperator<Map<String, String>>() {
            @Override
            public Map<String, String> operator(ShardedJedis sJedis) {
                return sJedis.hgetAll(key);
            }
        });
        Map<String, T> resultList = new LinkedHashMap<String, T>();
        for (Map.Entry<String, String> entry : result.entrySet()) {
            resultList.put(entry.getKey(), jsonDecode(entry.getValue(), type));
        }
        return resultList;
    }

    public <T> List<T> hvalsBin(final String key, final Class<T> type) {
        List<T> resultList = new ArrayList<T>();
        List<String> list = shardedJedisPoolClient.execute(new ShardedJedisOperator<List<String>>() {
            @Override
            public List<String> operator(ShardedJedis sJedis) {
                return sJedis.hvals(key);
            }
        });
        for (String s : list) {
            resultList.add(jsonDecode(s, type));
        }
        return resultList;
    }

    public long hlen(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.hlen(key);
            }
        });

    }

    public String hmsetBin(final String key, final Map<String, Object> hash) {
        Map<String, String> values = new HashMap<String, String>();
        for (Map.Entry<String, Object> entry : hash.entrySet()) {
            values.put(entry.getKey(), jsonEncode(entry.getValue()));
        }
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.hmset(key, values);
            }
        });
    }

    public <T extends Serializable> List<T> hmgetBin(final String key, final Class<T> type, final String... fields) {
        if (fields == null || fields.length == 0) {
            return new ArrayList<T>();
        }
        List<String> values = shardedJedisPoolClient.execute(new ShardedJedisOperator< List<String>>() {
            @Override
            public  List<String> operator(ShardedJedis sJedis) {
                return sJedis.hmget(key, fields);
            }
        });
        List<T> result = new ArrayList<T>(values.size());
        for (String value : values) {
            result.add(jsonDecode(value, type));
        }
        return result;
    }

    public String hmset(final String key, final Map<String, String> hash) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.hmset(key, hash);
            }
        });
    }

    public List<String> hmget(final String key, final String... fields) {
        if (fields == null || fields.length == 0) {
            return new ArrayList<String>();
        }
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<List<String>>() {
            @Override
            public List<String> operator(ShardedJedis sJedis) {
                return sJedis.hmget(key, fields);
            }
        });
    }

    public Long hincrby(final String key, final String field, final long value) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.hincrBy(key, field, value);
            }
        });
    }

    public Long hincrby(final String key, final String field, final int time, final long value) {

        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                boolean exist = true;
                long leftTime = sJedis.ttl(key);
                if (leftTime < 0) {
                    // 过期时间小于0说明key不存在
                    exist = false;
                }
                long num = sJedis.hincrBy(key, field, value);
                if (!exist) {
                    // 有效时间不为0且key为新插入则设置业务缓存的有效时间
                    sJedis.expire(key, time);
                }
                return num;

            }
        });
    }

    /**
     * 获取hash的<key,value> 的所有keys
     * @param key
     * @return
     */
    public Set<String> hkeys(String key){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Set<String>>() {
            @Override
            public Set<String> operator(ShardedJedis sJedis) {
                return sJedis.hkeys(key);
            }
        });
    }


    /**------------list类型-------------**/
    public <T> List<T> lrangeBin(final String key, final Class<T> type, final long start, final long end) {
        List<T> resultList = new ArrayList<T>();
        List<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator<List<String>>() {
            @Override
            public List<String> operator(ShardedJedis sJedis) {
                return sJedis.lrange(key, start, end);
            }
        });
        for (String s : results) {
            if(!s.isEmpty() && !"[]".equals(s)){
                resultList.add(jsonDecode(s, type));
            }
        }
        return resultList;
    }

    public Long lpushBin(final String key, final int time, final Object... objects) {
        if (objects == null || objects.length == 0) {
            return 0L;
        }
        final String[] jsonArray = objectsToJsonArray(objects);
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                sJedis.lpush(key, jsonArray);
                return sJedis.expire(key, time);
            }
        });
    }

    /*-----------------------------------------------------------
    特殊实现队列。支持查询队列是否存在的功能。
    ------------------------------------------------------------- */

    /**
     * 获取队列数据，如果队列不存在返回null， 如果队列为空，返回空list
     *
     * @param key key
     * @param start 开始位置
     * @param end 结束位置
     * @return 列表数据
     */
    public List<String> listRange(final String key, final long start, final long end) {
        List<String> list = shardedJedisPoolClient.execute(new ShardedJedisOperator<List<String>>() {
            @Override
            public List<String> operator(ShardedJedis sJedis) {
                return sJedis.lrange(key, start, end);
            }
        });
        return fromRangList(list);
    }

    public <T> List<T> listRangeBin(final String key, final Class<T> type, final long start, final long end) {
        List<String> list = shardedJedisPoolClient.execute(new ShardedJedisOperator<List<String>>() {
            @Override
            public List<String> operator(ShardedJedis sJedis) {
                return sJedis.lrange(key, start, end);
            }
        });
        return fromRangListBin(list, type);
    }

    public Long listLrem(final String key, final String value) {
        if (value == null || value.isEmpty()) {
            return 0L;
        }
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.lrem(key, 1, value);
        }
        });
    }

    public Long listLremBin(final String key, final Object value) {
        if (value == null) {
            return 0L;
        }
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.lrem(key, 1, jsonEncode(value));
            }
        });

    }

    /**
     * 创建空的队列
     *
     */
    public Long listCreate(final String key, final int time) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.expire(key, time);
            }
        });
    }

    public Long listLtrim(final String key, final int time, final int maxSize, final String... objects) {
        if (objects == null || objects.length == 0) {
            return 0L;
        }
        //      ArrayUtils.reverse(objects);
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                Long count = sJedis.lpush(key, objects);
                if (maxSize > 0) {
                    sJedis.ltrim(key, 0, maxSize - 1L);
                }
                sJedis.expire(key, time);
                return count;
            }
        });
    }

    public Long listLtrimBin(final String key, final int time, final int maxSize, final Object... objects) {
        if (objects == null || objects.length == 0) {
            return 0L;
        }
        final String[] jsonStringArray = objectsToJsonArrayReverse(objects);
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                Long count = sJedis.lpush(key, jsonStringArray);
                if (maxSize > 0) {
                    sJedis.ltrim(key, 0, maxSize - 1L);
                }
                sJedis.expire(key, time);
                return count;
            }
        });
    }

    public String lset(final String key, final long index, final Object object, final int time) {
        if (object == null) {
            return "";
        }
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                String res =  sJedis.lset(key, index, jsonEncode(object));
                sJedis.expire(key, time);
                return res;
            }
        });
    }

    /**
     * 功能描述: 获取list的长度<br>
     *
     * @param key
     */
    public Long llen(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.llen(key);
            }
        });
    }

    //从末尾压入元素
    public Long rpushBin(String key,int time,final Object... objects){
        if (objects == null || objects.length == 0) {
            return 0L;
        }
        final String[] jsonArray = objectsToJsonArray(objects);
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                Long result =  sJedis.rpush(key,jsonArray);
                if(time > 0){
                    sJedis.expire(key, time);
                }
                return result;
            }
        });
    }

    //ltrim,删除指定范围内以外的元素
    public String ltrim(String key,int start,int end){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.ltrim(key,start,end);
            }
        });
    }

    //lpop，从list的头部删除元素
    public String lpop(String key){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.lpop(key);
            }
        });
    }

    public String rpop(String key){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.rpop(key);
            }
        });
    }

    public String lindex(String key,int index){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.lindex(key,index);
            }
        });
    }

    /**------------set类型-------------**/

    public Long sadd(final String key, final String... values){
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.sadd(key, values);
            }
        });
    }

    public String spop(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.spop(key);
            }
        });
    }

    public Long scard(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.scard(key);
            }
        });
    }

    public long sremBin(final String key, final Object object) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.srem(key, jsonEncode(object));
            }
        });
    }

    public long srem(final String key, final String value) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.srem(key, value);
            }
        });
    }

    public <T> Set<T> smembersBin(final String key, final Class<T> type) {
        Set<T> result = new HashSet<T>();
        Set<String> sets = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.smembers(key);
            }
        });
        for (String set : sets) {
            result.add(jsonDecode(set, type));
        }
        return result;
    }

    public Set<String> smembers(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.smembers(key);
            }
        });
    }

    public Boolean sismember(final String key, final String value) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Boolean>() {
            @Override
            public  Boolean operator(ShardedJedis sJedis) {
                return sJedis.sismember(key, value);
            }
        });
    }


    /**------------ZSet类型-------------**/
    public <T> Set<T> zrangeByScore(final String key, final Class<T> type, final double min, final double max,
                                    final int offset, final int count) {
        Set<T> resultList = new LinkedHashSet<T>();
        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrangeByScore(key, min, max, offset, count);
            }
        });
        for (String result : results) {
            resultList.add(jsonDecode(result, type));
        }
        return resultList;
    }

    public <T> Set<T> zrangeByScore(final String key, final Class<T> type, final String min, final String max,
                                    final int offset, final int count) {
        Set<T> resultList = new LinkedHashSet<T>();
        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrangeByScore(key, min, max, offset, count);
            }
        });
        for (String result : results) {
            resultList.add(jsonDecode(result, type));
        }
        return resultList;
    }

    public <T> Set<T> zrevrangeByScore(final String key, final Class<T> type, final double max, final double min,
                                       final int offset, final int count) {
        Set<T> resultList = new LinkedHashSet<T>();
        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeByScore(key, min, max, offset, count);
            }
        });
        for (String result : results) {
            resultList.add(jsonDecode(result, type));
        }
        return resultList;
    }

    public <T> Set<T> zrevrangeByScore(final String key, final Class<T> type, final String max, final String min,
                                       final int offset, final int count) {
        Set<T> resultList = new LinkedHashSet<T>();
        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeByScore(key, max, min, offset, count);
            }
        });
        for (String result : results) {
            resultList.add(jsonDecode(result, type));
        }
        return resultList;
    }

    //返回Set(String)
    public Set<String> zrevrangeByScore1(final String key, final String max, final String min,
                                         final int offset, final int count) {
        Set<String> resultList = new LinkedHashSet<String>();
        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeByScore(key, max, min, offset, count);
            }
        });
        for (String result : results) {
            resultList.add(result);
        }
        return resultList;
    }
    //返回Set(String)
    public Set<String> zrevrangeByScore1(final String key, final double max, final double min,
                                         final int offset, final int count) {
        Set<String> resultList = new LinkedHashSet<String>();
        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeByScore(key, max, min, offset, count);
            }
        });
        for (String result : results) {
            resultList.add(result);
        }
        return resultList;
    }

    public <T> Set<T> zrevrangeByScore(final String key, final Class<T> type, final double max, final double min) {
        Set<T> resultList = new LinkedHashSet<T>();
        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeByScore(key, max, min);
            }
        });
        for (String result : results) {
            resultList.add(jsonDecode(result, type));
        }
        return resultList;
    }

    public <T> Set<T> zrevrange(final String key, final Class<T> type, final int start, final int end) {
        Set<T> resultList = new LinkedHashSet<T>();
        Set<String> results =  shardedJedisPoolClient.execute(new ShardedJedisOperator< Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrevrange(key, start, end);
            }
        });
        for (String result : results) {
            resultList.add(jsonDecode(result, type));
        }
        return resultList;
    }

    /**
     * 功能描述: 添加元素，并且保证最大maxSize条数<br>
     * @param maxSize 最大maxSize条数
     */
    public Long zaddMap(final String key, final int maxSize, final Map<String, Double> objects) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                sJedis.zadd(key, objects);
                Long cnt = zcard(key);
                if (cnt > maxSize) {
                    sJedis.zremrangeByRank(key, 0, cnt - maxSize - 1);
                }
                return cnt;
            }
        });
    }

    public Long zcard(final String key) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zcard(key);
            }
        });
    }

    public Set<String> zrevrange(final String key, final long start, final long end) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Set<String>>() {
            @Override
            public Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrevrange(key, start, end);
            }
        });
    }

    public Set<Tuple> zrevrangeWithScores(final String key, final long start, final long end) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Set<Tuple>>() {
            @Override
            public Set<Tuple> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeWithScores(key, start, end);
            }
        });
    }

    /**
     * added by 功能描述: 获取带分值对象
     */
    public Set<Tuple> zrevrangeByScoreWithScores(final String key, final String max, final String min,
                                                 final int offset, final int count) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Set<Tuple>>() {
            @Override
            public Set<Tuple> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeByScoreWithScores(key, max, min, offset, count);
            }
        });
    }

    /**
     * added by 功能描述: 获取带分值对象
     */
    public Set<Tuple> zrevrangeByScoreWithScores(final String key, final double max, final double min,
                                                 final int offset, final int count) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Set<Tuple>>() {
            @Override
            public Set<Tuple> operator(ShardedJedis sJedis) {
                return sJedis.zrevrangeByScoreWithScores(key, max, min, offset, count);
            }
        });
    }

    public Long zremrangeByScore(final String key, final double start, final double end) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zremrangeByScore(key, start, end);
            }
        });
    }

    public Long zrem(final String key, final Object object) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zrem(key, jsonEncode(object));
            }
        });
    }

    public Double zscore(final String key, final String member) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Double>() {
            @Override
            public Double operator(ShardedJedis sJedis) {
                return sJedis.zscore(key, member);
            }
        });
    }

    public double zincrby(final String key, final double score, final String member) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Double>() {
            @Override
            public Double operator(ShardedJedis sJedis) {
                return sJedis.zincrby(key, score, member);
            }
        });
    }

    /*
     * 有序集合（Sorted Sets）
     */
    public long zadd(final String key, final double score, final String value) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zadd(key, score, value);
            }
        });
    }

    /*
     * 有序集合（Sorted Sets）
     */
    public Long zadd(final String key, final double score, final Object member) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zadd(key, score, jsonEncode(member));
            }
        });

    }
    /**
     * hashSet类型，根据key和member 判定member是否存在
     *
     * @param key redis key
     * @param member 0 最大 nil 表示没有
     */
    public Long zrevrank(final String key, final String member) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zrevrank(key, member);
            }
        });
    }
    /**
     * SortSet类型，根据key和member 判定member是否存在
     *
     * @param key redis key
     * @param member 0 最大 nil 表示没有
     */
    public Long zrank(final String key, final String member) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zrank(key, member);
            }
        });

    }

    public Long zcount(final String key, final double min, final double max) {
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Long>() {
            @Override
            public Long operator(ShardedJedis sJedis) {
                return sJedis.zcount(key, min, max);
            }
        });
    }

    public <T> Set<T> zrange(final String key, final Class<T> type, final int offset, final int count) {
        Set<T> resultList = new LinkedHashSet<T>();

        Set<String> results = shardedJedisPoolClient.execute(new ShardedJedisOperator<Set<String>>() {
            @Override
            public  Set<String> operator(ShardedJedis sJedis) {
                return sJedis.zrange(key, offset, count);
            }
        });
        for (String result : results) {
            resultList.add(jsonDecode(result, type));
        }
        return resultList;
    }

    /**--------------------ShardedJedisPipeline 管道技术---------------------**/

    /**
     * ZSet，批量获取一批成员的排名
     */
    public List<Long> batchZrank(final String key, final List<String> values) {
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        for (String value : values) {
            pipeline.zrank(key, value);
        }
        List<Object> list = pipeline.syncAndReturnAll();
        List<Long> result = new ArrayList<Long>();
        for (Object item : list) {
            result.add((Long) item);
        }
        return result;
    }

    /**
     * Set类型  批量判断是否存在成员
     */
    public List<Boolean> batchSismember(final String key, final List<String> values) {
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        for (String value : values) {
            pipeline.sismember(key, value);
        }
        List<Object> list = pipeline.syncAndReturnAll();
        List<Boolean> result = new ArrayList<Boolean>();
        for (Object item : list) {
            result.add((Boolean) item);
        }
        return result;
    }

    /**
     * String类型  批量获取信息
     */
    public <T extends Serializable> List<T> batchGet(final Class<T> type, final String... keys) {
        if (keys == null || keys.length == 0) {
            return new ArrayList<T>();
        }
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });

        for (String key : keys) {
            pipeline.get(key);
        }
        List<Object> responses = pipeline.syncAndReturnAll();
        List<T> result = new ArrayList<T>(responses.size());
        for (Object response : responses) {
            String resp = (String) response;
            result.add(resp == null ? null : jsonDecode(resp, type));
        }
        return result;
    }

    /**
     * String类型  批量存储信息
     * @param hash
     * @param time
     */
    public void batchSet(final Map<String, Object> hash, final int time) {
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });

        for (Map.Entry<String, Object> entry : hash.entrySet()) {
            pipeline.setex(entry.getKey(), time, jsonEncode(entry.getValue()));
        }
        pipeline.sync();
    }


    /**
     * hash类型  批量存储信息
     */
    public void batchHset(final Map<String, Map<String,String>> hashMap){
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        for (Map.Entry<String, Map<String,String>> entry : hashMap.entrySet()) {
            String key = entry.getKey();
            Map<String,String> fieldMap = entry.getValue();
            pipeline.hmset(key,fieldMap);
        }
        pipeline.sync();
    }

    /**
     * hash类型  批量获取
     */
    public Map<String,List<String>> batchHget(final LinkedList<String> keys,final String[] fields){
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        for (String key : keys) {
            pipeline.hmget(key,fields);
        }
        List<Object> responses = pipeline.syncAndReturnAll();

        int i = 0;
        Map<String,List<String>> resultMap = new LinkedHashMap<>();
        for (Object response : responses) {
            List<String> resp = (List<String>) response;
            String v1 = resp.get(0);
            String v2 = resp.get(1);
            resultMap.put(keys.get(i++),resp);
        }
        return resultMap;

    }


    public List<List<String>> batchListRange(final List<String> keys, final long start, final long end) {
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        for (String key : keys) {
            pipeline.lrange(key, start, end);
        }
        List<Object> list = pipeline.syncAndReturnAll();
        List<List<String>> result = new ArrayList<List<String>>();
        for (Object item : list) {
            result.add(fromRangList((List<String>) item));
        }
        return result;
    }


    /**
     * list类型  批量获取信息
     * @param
     */
    public <T> List<List<T>> batchListRangeBin(final List<String> keys, final Class<T> type, final long start,
                                               final long end) {
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        List<Response<List<String>>> resps = new LinkedList<>();
        for (String key : keys) {
            Response<List<String>> resp = pipeline.lrange(key, start, end);
            resps.add(resp);
        }
        pipeline.sync();
        List<List<T>> result = new LinkedList<List<T>>();
        for (Response<List<String>> resp : resps) {
            List<T> binList = fromRangListBin(resp.get(), type);
            result.add(binList);
        }
        return result;
    }


    /**
     * list类型  批量存储信息
     * @param hashMap
     */
    public <T> void batchListSet(final Map<String, List<T>> hashMap){

        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        for (Map.Entry<String, List<T>> entry : hashMap.entrySet()) {
            String key = entry.getKey();
            List<T> values = entry.getValue();
            String[] result = new String[values.size()];
            int i = 0;
            for (T temp : values) {
                result[i++] = jsonEncode((Object) temp);
            }
            pipeline.lpush(key, result);
        }
        pipeline.sync();

    }

    /**
     * SortSet 类型，根据key和member,批量获取member的score值
     */
    public List<Double> batchZscore(final String key, final List<String> values) {
        ShardedJedisPipeline pipeline = shardedJedisPoolClient.execute(new ShardedJedisOperator<ShardedJedisPipeline>() {
            @Override
            public ShardedJedisPipeline operator(ShardedJedis sJedis) {
                return sJedis.pipelined();
            }
        });
        for (String value : values) {
            pipeline.zscore(key, value);
        }
        List<Object> list = pipeline.syncAndReturnAll();
        List<Double> result = new ArrayList<Double>();
        for (Object item : list) {
            result.add((Double) item);
        }
        return result;
    }


    /**--------------------分布式锁---------------------**/
    /**
     * 【有问题】
     * @param lockname 锁名
     * @param seconds 锁失效时长
     */
    public boolean acquireLock(final String lockname, final int seconds) {
        if (exists(getLockKey(lockname))) {
            return false;
        }
        String lockKey = getLockKey(lockname);
        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Boolean>() {
            @Override
            public Boolean operator(ShardedJedis sJedis) {
                Jedis jedis = sJedis.getShard(lockname);
                Transaction transaction = jedis.multi();
                transaction.setnx(lockKey, "lock");
                transaction.expire(lockKey, seconds);
                List<Object> rets = transaction.exec();
                long ret = (Long) rets.get(0);
                return ret == 1;
            }
        });

    }

    /**
     *应用在分布式锁2
     *  value 很重要，可以根据具体请求者设置不同值，来确定唯一性
     *  NX：当有key存在，不会设置成功；当不存在，就可以抢占。--只有一个客户端获取到锁，互斥性
     *  PX:设置了过期标识，即时某个用户奔溃而没解锁，也不会引起死锁，会自动解锁。
     *
     *  缺点：需要事先预估执行时间，以便设置的过期时间内执行完相关操作
     */
    public boolean acquireLock2(final String lockname,final String value, final int seconds) {
        String result =  shardedJedisPoolClient.execute(new ShardedJedisOperator<String>() {
            @Override
            public String operator(ShardedJedis sJedis) {
                return sJedis.set(lockname,value,"NX","PX",seconds);
            }
        });
        return "OK".equals(result);
    }

    /**
     * 释放锁: <br>
     * @param lockname 分布式锁Key
     */
    public void releaseLock(final String lockname) {
        String lockKey = getLockKey(lockname);
        del(lockKey);
    }

    public String getLockKey(String lockname) {
        return "lock_".concat(lockname);
    }

    /**
     * 这个只针对同一个key的操作，执行lua脚本实现分布式锁功能
     */
    public Object evalWayV1(String bKey, String evalLua,List<String> keys,List<String> args){

        return shardedJedisPoolClient.execute(new ShardedJedisOperator<Object>() {
            @Override
            public Object operator(ShardedJedis sJedis) {
                return sJedis.getShard(bKey).eval(evalLua,keys,args);
            }
        });
    }

}