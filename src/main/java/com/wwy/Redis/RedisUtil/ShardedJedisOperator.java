package com.wwy.Redis.RedisUtil;

import redis.clients.jedis.ShardedJedis;

/**
 * 具体操作命令接口，执行的是set、get具体命令
 */
public interface ShardedJedisOperator<T> {
    T operator(ShardedJedis sJedis);
}
