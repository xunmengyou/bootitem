package com.wwy.Redis.RedisUtil;

/**
 * Pool接收到命令请求。 getResource()---执行命令---回收close()
 */
public interface ShardedJedisPoolClient {

    <T> T execute(ShardedJedisOperator<T> parameter);
}
