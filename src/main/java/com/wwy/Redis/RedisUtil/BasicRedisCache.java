package com.wwy.Redis.RedisUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  * 1) 读取 -(未找到)-> 回源 -> 写入 -> 返回
 * 2) 读取 -(找到)-> 返回
 *
 * @param <T> 输出类型
 * @param <Z> 读写的输入参数类型
 */
public abstract class BasicRedisCache<T,Z> {

    @Resource(name = "jedisCacheUtil")
    public JedisCacheUtil jedisCacheUtil;


    /**
     * 读Redis缓存的实现
     *StringRedisCacheWorker
     * @param z 输入参数
     * @return
     */
    protected T read(Z z) {
        throw new IllegalArgumentException("read 方法未定义" + z);
    }

    /**
     * 批量读redis缓存的实现
     *
     * @param z 输入参数
     * @return 输出读取结果
     */
    protected List<T> batchRead(List<Z> z) {
        throw new IllegalArgumentException("batchRead 方法未定义" + z);
    }

    /**
     * 回源并写Redis缓存的实现
     *
     * @param z 输入参数
     * @return 输出回源结果
     */
    protected T write(Z z) {
        throw new IllegalArgumentException("write 方法未定义" + z);
    }

    /**
     * 批量回源并写Redis缓存的实现
     *
     * @param z 输入参数
     * @return 输出回源结果
     */
    private Map<Z, T> batchWrite(List<Z> z, List<Integer> pos) {
        // 1.
        List<Z> keyOfNeedWriteList = new ArrayList<Z>(pos.size());
        for (Integer i : pos) {
            keyOfNeedWriteList.add(z.get(i));
        }
        return batchWrite(keyOfNeedWriteList);
    }

    protected Map<Z, T> batchWrite(List<Z> z) {
        throw new IllegalArgumentException("batchWrite 方法未定义");
    }


    /**
     * 单条回源后，立即执行方法入口
     *
     * @param t 输入对象
     * @return 输出对象
     */
    protected T beforeReturn(T t) {
        return t;
    }

    /**
     * 批量回源后，立即执行方法入口
     *
     * @param list 输入对象
     * @return 输出对象
     */
    protected List<T> beforeBatchReturn(List<T> list) {
        return list;
    }

    /**
     * 执行批量查询的方法
     *
     * @param list 输入参数
     * @return 结果
     */
    @SuppressWarnings("all")
    public List<T> batchFind(List<Z> list) {
        if (list == null || list.isEmpty()) {
            return new ArrayList<T>();
        }

        List<T> tList = batchRead(list);
        List<Integer> posList = nullPos(tList);
        if (!posList.isEmpty()) {
            Map<Z, T> tmpList = batchWrite(list, posList);
            for (int i = 0; i < posList.size(); i++) {
                int pos = posList.get(i);
                tList.set(pos, tmpList.get(list.get(pos)));
            }
        }
        return beforeBatchReturn(tList);

    }

    /**
     * 执行单条查询的方法
     *
     * @param z 输入参数
     * @return 结果
     */
    @SuppressWarnings("all")
    public T find(Z z) {
        if (z == null) {
            return null;
        }
        T t = read(z);
        if (t == null) {
            t = write(z);
        }
        return beforeReturn(t);
    }

    /**
     * 计算列表中为null的各元素位置
     *
     * @param t 列表
     * @return null元素位置列表
     */
    private List<Integer> nullPos(List<T> t) {
        List<Integer> posList = new ArrayList<Integer>();
        for (int i = 0; i < t.size(); i++) {
            if (t.get(i) == null) {
                posList.add(i);
            }
        }
        return posList;
    }

}
