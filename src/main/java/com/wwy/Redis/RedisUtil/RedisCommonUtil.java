package com.wwy.Redis.RedisUtil;

import com.alibaba.fastjson.JSON;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RedisCommonUtil {

    public String jsonEncode(Object object) {
        if (object == null) {
            return null;
        }
        return JSON.toJSONString(object);
    }

    public <T> T jsonDecode(String input, Class<T> type) {
        if (input == null || input.length() == 0) {
            return null; // NOSONAR
        }
        return JSON.parseObject(input, type);
    }

    public <T> List<T> jsonDecodeArray(String input, Class<T> type) {
        if (input == null || input.length() == 0) {
            return null; // NOSONAR
        }
        return JSON.parseArray(input, type);
    }

    /**
     * list to array
     */
    public String[] objectsToJsonArray(Object... objects) {
        int length = objects.length;
        String[] result = new String[length];
        for (int i = 0; i < length; i++) {
            result[i] = jsonEncode(objects[i]);
        }
        return result;
    }

    public String[] objectsToJsonArrayReverse(Object... objects) {
        int length = objects.length;
        String[] result = new String[length];
        for (int i = 0; i < length; i++) {
            result[i] = jsonEncode(objects[length - i - 1]);
        }
        return result;
    }

    public String key(String key, String flagId) {
        StringBuilder sb = new StringBuilder();
        return sb.append(key).append(flagId).toString();
    }

    public String key(String key, String flagId, String flagId2) {
        StringBuilder sb = new StringBuilder();
        return sb.append(key).append(flagId).append(":").append(flagId2).toString();
    }

    /**
     * 功能描述: 以天为单位组key<br>
     */
    public String keyByDate(String key) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");// 设置日期格式
        StringBuilder sb = new StringBuilder();
        return sb.append(key).append(df.format(new Date())).toString();
    }

    public String keyByDay(String key, String uId) {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        StringBuilder sb = new StringBuilder();
        return sb.append(key).append(day).append(uId).toString();
    }

    /**
     *获取随机的缓存失效时间<br>
     */
    public int setExpireTime(int rand) {
        int day = (int) (1 + Math.random() * rand);
        //    return day * RedisConstants.DAYTIME;
        return day * 10;
    }

    public List<String> fromRangList(List<String> list) {
        List<String> result = new ArrayList<String>();
        int size = list.size();
        if (size == 0) {
            return null; // NOSONAR
        }
        if (list.get(0).isEmpty()) {
            return result;
        }
        for (String s : list) {
            if (!s.isEmpty()) {
                result.add(s);
            }
        }
        return result;
    }

    public <T> List<T> fromRangListBin(List<String> list, Class<T> type) {
        List<T> result = new ArrayList<T>();
        int size = list.size();
        if (size == 0) {
            return null;// NOSONAR
        }
        if (list.get(0).isEmpty()) {
            return result;
        }
        for (String s : list) {
            if (!s.isEmpty()) {
                result.add(jsonDecode(s, type));
            }
        }
        return result;
    }
}
