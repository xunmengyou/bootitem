package com.wwy.Redis;

import com.wwy.Redis.RedisUtil.BasicRedisCache;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * string 类型的值最大能存储 512MB
 *
 *<T,Z>第一个参数T是处理后的返回值Object/Bean对象，第二个参数Z一般是key
 * 这里的思路是：传来对应的key，StringRedisCacheWorker类里面可以查MySQL来
 *               组装数据，然后存入Redis中且也返回一份
 */
@Service
public class StringRedisCacheWorker extends BasicRedisCache<String,String> {


    @Override
    public String write(String key){
        //查数据库，将值存入Redis，并返回
        String nCount = "";

        //写数据 【无论key是否有数据，都可以set，且成功返回OK】
        String value1 = jedisCacheUtil.set(key, nCount);//缓存中是0
        String value = jedisCacheUtil.get(key);

        value1 = jedisCacheUtil.set(key, "2");

        jedisCacheUtil.setBin(key+"Bin",nCount); //会转换为Json存储,是"0"
        jedisCacheUtil.setexBin(key+"exBin",nCount,1*60*60); //10秒后，这个key就会消失
        //获取数据
   //     String value = jedisCacheUtil.get(key);//value为"0"
        //追加数据(append) 如果当前key的value有值则附加到原有string后面，如果没有则写入
        Long lResult = jedisCacheUtil.append(key,"2"); //缓存中是0"2" ，lResult为4
        //删除数据
        lResult =  jedisCacheUtil.del(key+"Bin"); //lResult为1
       //getSet 获取原有value值的同时写入新的value值
        value = jedisCacheUtil.getSet(key,"29");//value返回"0"2"",现在缓存中是29
       //incr，incrby，数据加法运算,incr为+1内置运算，incrby为+n自设n运算
           //“value数据不是整型或数据超出64位有符号整形数据范围”，会报错
           //由于不存在age的key与value值，但是默认age为key值为0进行+1运算。
        lResult = jedisCacheUtil.incr(key); //lResult为30
        lResult = jedisCacheUtil.incr(key,10);//lResult为31，又将这个key设置10秒后消失
        lResult = jedisCacheUtil.incrBy(key,10,2L);//当这个key不存在，由0加2L，返回2,10秒后消失
      //decr，decrby, 数据减法运算，decr为-1内置运算，decrby为减n自设n运算
          //同上类似操作
        lResult = jedisCacheUtil.decr(key,10);//-1,10秒后消失
        lResult = jedisCacheUtil.decr(key);//-1，不消失
        //------注意incr,decr等操作，都有时间设置，所以调试时是有差别的
        //string长度的命令strlen,如果该key或者value不存在，则返回0
        lResult = jedisCacheUtil.strlen(key);//"-1"的话，对应长度为2
        lResult = jedisCacheUtil.strlen(key+"1");//没有key,为0
       //判断原值是否存在，存在不赋值，返回0；不存在才赋值，返回1；命令setnx
        lResult = jedisCacheUtil.setnx(key,"38");
        lResult = jedisCacheUtil.setnx(key+"1","38");
       //字符串替换赋值，从指定位置开始替换，命令setrange
        lResult = jedisCacheUtil.setrange(key,1,"777");//原来是"-1"，现在是"-777"，lResult为4
       //截取字符串，从下标为n开始截取到n或n+1，getrange
        value = jedisCacheUtil.getrange(key,1,20);//value为777
      //批量操作修改及读取string数据，命令mget，批量读取，命令mset，批量赋值，命令msetnx，
       //带事务性的赋值，发现有一个key已经存在，所有事务回归，不做赋值处理操作
//        LinkedHashMap<String,Object> mapStr = new LinkedHashMap<>();
//        mapStr.put(key,999);
//        mapStr.put(key+"exBin",888);
//        jedisCacheUtil.msetBin(mapStr,60); //就是批量的setBin操作

        String[] keys={"1","exBin"};
        List<String> listLog = jedisCacheUtil.batchGet(String.class,keys); //因为都没值了，所以list为null
        return "";
    }
}
